// @flow
import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator, Alert, ScrollView, Button } from 'react-native'
import { Currency } from '../components/Currency'

type CurrencyType = { name: string, price: { amount: number }, volume: number }
type NavigationType = {
  setParams: Function,
  getParam: Function
}
type Props = { navigation: NavigationType }
type State = { data: Array<CurrencyType>|null, status: 'loading'|null }

class Home extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    title: 'Главный экран',
    headerRight: (
      <Button
        onPress={() => navigation.getParam('fetchdata')()}
        title="Загрузить"
      />
    )
  })

  state = {
    data: null,
    status: null
  }

  async componentDidMount() {
    this.props.navigation.setParams({ fetchdata: this.fetchdata })
    await this.fetchdata()
    setInterval(this.fetchdata, 15000)
  }

  fetchdata = async () => {
    this.setState(state => ({ ...state, status: 'loading' }))

    try {
      const response = await fetch('http://phisix-api3.appspot.com/stocks.json')
      const result = await response.json()

      this.setState({ data: result.stock, status: null })
    } catch (e) {
      this.setState({ data: null, status: null})
      Alert.alert('Ошибка', 'Повторите попытку позже')
    }

    return Promise.resolve()
  }

  render() {
    return (
      <View style={styles.container}>

        {this.state.data && (
          <ScrollView>
            {this.state.data.map(Currency)}
          </ScrollView>
        )}

        {this.state.status === 'loading' && (
          <ActivityIndicator
            size="large"
            color="#0c0c0c"
            style={styles.loader}
          />
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  loader: {
    position: 'absolute',
    width: '100%'
  }
})

export { Home }
