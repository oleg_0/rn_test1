import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

type CurrencyType = { name: string, price: { amount: number }, volume: number }

export const Currency = (props: CurrencyType) => (
  <View key={props.name} style={styles.currency}>
    <Text style={styles.name}>{props.name}</Text>
    <Text style={styles.amount}>
      {props.price.amount.toLocaleString('ru-RU', {
        style: 'decimal',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      })}
    </Text>
    <Text style={styles.volume}>{props.volume}</Text>
  </View>
)

const styles = StyleSheet.create({
  currency: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  name: {
    paddingLeft: 8
  },
  amount: {
    width: 100,
    marginLeft: 'auto',
    textAlign: 'center'
  },
  volume: {
    width: 100,
    textAlign: 'center',
    paddingRight: 8
  }
})
