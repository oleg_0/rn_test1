import React from 'react'
import { createStackNavigator } from 'react-navigation'
import { Home } from './routes/Home'

export default createStackNavigator({
  Home: {
    screen: Home
  },
})
